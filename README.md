# knapp_eval

A ROS2 package in fulfilment to the candidate evaluation task

**Dependency**:

Install yaml-cpp as this package uses it to parse yaml files.

```bash
    sudo apt-get install libyaml-cpp-dev
```

The Package contains a ROS2 node named as OrderOptimizer 

The node subscribes to topics:
<ul>
<li> currentPosition</li>
<li> nextOrder </li>
</ul>

and publishes to the topic 'order_path'.

To run the ros2 node use command mentioned below:

```bash
    ros2 run knapp_eval order_opt_node --ros-args -p config_file_path:="/home/kapil/Documents/applicants_amr_example_1"
```

Use the config_file_path as a parameter to give path of configuration and order folder and files.

This node has completed the tasks mentioned below:

<ol>
<li>Made ros2 publishers and subscribers for above mentioned topics</li>
<li>Made a custom ROS message Order.msg</li>
<li>Parsed the products.yaml file successfully to save it in C++ map</li>
<li>Parsed order*.yaml file successfully to find the order id, but the experiment of multi-threading did not work but, approach for the same has been still mentioned in the Optimizer.cpp file</li>
<li>The node finds an optimized route to fetch different parts mentioned in order, the algorithm used is a very simple algorithm which finds nearest neighbour for each waypoint. Most efficient algorithm for 2D Euclidian path found was bitonic tour but it was getting too complex to implement it.</li>
<li> The node also publishes marker array for current position and the optimized route. </li>
</ol>

Results: 

1. 
![](doc/node-terminal.png)
 **figure 1** OrderOptimizer Terminal Window

2.
![](doc/rqt.png)
**figure 2** RQT Window to publish currentPosition and nextOrder topics

3.
![](doc/order-path-pub.png)

**figure 3** ROS2 topic echo of order_path topic