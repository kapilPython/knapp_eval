#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "knapp_eval/msg/order.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "geometry_msgs/msg/pose.hpp"
#include "visualization_msgs/msg/marker_array.hpp"
#include "visualization_msgs/msg/marker.hpp"
//#include "knapp_eval/ProductDB.h"
#include "knapp_eval/Optimizer.h"
#include <mutex>

using std::placeholders::_1;

class OrderOptimization : public rclcpp::Node
{
    public:
        OrderOptimization()
        : Node("OrderOptimizer")
        {
            //parameter handling and initialization
            this->declare_parameter("config_file_path");
            rclcpp::Parameter config_files = this->get_parameter("config_file_path");
            complete_file_path_ = config_files.as_string();
            /*pdb_.create_database(complete_file_path_);
            auto product_sample = pdb_.getProduct(2);
            RCLCPP_INFO(this->get_logger(),"Product: %s",product_sample.prod_name_.c_str());*/
            opt_ = new Optimizer(complete_file_path_);

            // callback group to run each subscriber on separate threads
            callback_group_pose_sub_ = this->create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);
            callback_group_new_order_ = this->create_callback_group(rclcpp::CallbackGroupType::MutuallyExclusive);
            
            auto pose_sub_opt_ = rclcpp::SubscriptionOptions();
            pose_sub_opt_.callback_group = callback_group_pose_sub_;
            
            auto new_order_opt_ = rclcpp::SubscriptionOptions();
            new_order_opt_.callback_group = callback_group_new_order_;

            // currentPosition Subscriber
            pose_subscription_ = this->create_subscription<geometry_msgs::msg::PoseStamped>(
                "currentPosition", 10, std::bind(&OrderOptimization::pose_data_callback,this, _1), pose_sub_opt_
            );

            // nextOrder Subscriber
            new_order_subscription_ = this->create_subscription<knapp_eval::msg::Order>(
                "nextOrder", 10, std::bind(&OrderOptimization::new_order_callback,this, _1),new_order_opt_
            );

            // order_path publisher
            visu_publisher_ = this->create_publisher<visualization_msgs::msg::MarkerArray>("order_path",10);

        }
    private:
        // callback for currentPosition Subscriber
        void pose_data_callback(geometry_msgs::msg::PoseStamped::SharedPtr pose_)
        {
            RCLCPP_INFO(this->get_logger(),"X: %f", pose_->pose.position.x);
            RCLCPP_INFO(this->get_logger(),"Y: %f", pose_->pose.position.y);
            mtx.lock();
            pose_saved_.set__position(pose_->pose.position);
            pose_saved_.set__orientation(pose_->pose.orientation);
            mtx.unlock();
        }

        // Function to create visualization markers for initial position and order path
        visualization_msgs::msg::Marker create_visu_marker(bool currPos, 
                                                            float pose_x, 
                                                            float pose_y)
        {
            visualization_msgs::msg::Marker mkCurrPose;
            mkCurrPose.header.frame_id = "map";
            mkCurrPose.header.stamp = rclcpp::Time();
            mkCurrPose.ns = "/";
            mkCurrPose.id = 0;
            if(currPos){
                mkCurrPose.type = visualization_msgs::msg::Marker::CUBE;
            }
            else{
                mkCurrPose.type = visualization_msgs::msg::Marker::CYLINDER;
            }
            mkCurrPose.action = visualization_msgs::msg::Marker::ADD;
            mkCurrPose.pose.position.x = pose_x;
            mkCurrPose.pose.position.y = pose_y;
            mkCurrPose.pose.position.z = 1;
            mkCurrPose.pose.orientation.x = 0.0;
            mkCurrPose.pose.orientation.y = 0.0;
            mkCurrPose.pose.orientation.z = 0.0;
            mkCurrPose.pose.orientation.w = 1.0;
            mkCurrPose.scale.x = 1;
            mkCurrPose.scale.y = 0.1;
            mkCurrPose.scale.z = 0.1;
            mkCurrPose.color.a = 1.0; // Don't forget to set the alpha!
            mkCurrPose.color.r = 0.0;
            mkCurrPose.color.g = 1.0;
            mkCurrPose.color.b = 0.0;
            return mkCurrPose;
        }

        // callback for nextOrder Subscriber
        void new_order_callback(knapp_eval::msg::Order::SharedPtr next_order_)
        {
            RCLCPP_INFO(this->get_logger(),"Order ID: %u", next_order_->order_id);
            RCLCPP_INFO(this->get_logger(),"Description: %s", next_order_->description.c_str());
            mtx.lock();
            float curr_pose_x = pose_saved_.position.x;
            float curr_pose_y = pose_saved_.position.y;
            mtx.unlock();
            auto opt_path = opt_->path_optimizer(next_order_->order_id,
                                                    curr_pose_x,
                                                    curr_pose_y);
            RCLCPP_INFO(this->get_logger(),"Working on order %u %s",next_order_->order_id,next_order_->description.c_str());
            for(auto optProdPart: opt_path){
                RCLCPP_INFO(this->get_logger(),"Fetching part %s for %s at x: %f , y: %f",optProdPart.part_name.c_str(),optProdPart.prod_name.c_str(),optProdPart.pose_x,optProdPart.pose_y);
            }

            visualization_msgs::msg::MarkerArray mka;
            visualization_msgs::msg::Marker mkCurrPose = create_visu_marker(true,curr_pose_x,curr_pose_y);
            mka.markers.push_back(mkCurrPose);

            for (auto optProdPart: opt_path){
                visualization_msgs::msg::Marker mkOrderPose = create_visu_marker(false,optProdPart.pose_x,optProdPart.pose_y);
                mka.markers.push_back(mkOrderPose);
            }

            visu_publisher_->publish(mka);
            /*if(opt_->path_optimizer(next_order_->order_id,
                                    curr_pose_x,
                                    curr_pose_y)
            )
            {
                RCLCPP_INFO(this->get_logger(),"Order Found");
            }
            else{
                RCLCPP_INFO(this->get_logger(),"Order Not Found");
            }*/
        }

        std::mutex mtx; // to lock and unlock usage of pose_saved_ data as it is being shared by two threads
        std::string complete_file_path_;
        geometry_msgs::msg::Pose pose_saved_;
        //ProductDB pdb_;
        Optimizer* opt_; // Object of Optimizer class
        rclcpp::CallbackGroup::SharedPtr callback_group_pose_sub_;
        rclcpp::CallbackGroup::SharedPtr callback_group_new_order_;
        rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr visu_publisher_;
        rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr pose_subscription_;
        rclcpp::Subscription<knapp_eval::msg::Order>::SharedPtr new_order_subscription_;
};

int main(int argc, char ** argv)
{
  (void) argc;
  (void) argv;

  rclcpp::init(argc,argv);
  rclcpp::spin(std::make_shared<OrderOptimization>());
  rclcpp::shutdown();
  return 0;
}