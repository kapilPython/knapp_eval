#include "knapp_eval/ProductDB.h"
#include "yaml-cpp/yaml.h"
#include "yaml-cpp/exceptions.h"

ProductDB::ProductDB(){
    /*
     Nothing To Do
    */
}

void ProductDB::create_database(std::string path){
    std::string final_path = path + "/configuration/products.yaml";
    YAML::Node product_data = YAML::LoadFile(final_path);
    for (unsigned int i = 0; i < product_data.size(); i++)
    {
        ProductPtr p = ProductPtr(new ProductDB::Product());
        p->addProdName(((product_data[i])["product"]).as<std::string>());
        YAML::Node parts_data = product_data[i]["parts"];
        for (unsigned int j = 0; j < parts_data.size(); j++){
            p->addPart(
            ((parts_data[j])["part"]).as<std::string>(),
            ((parts_data[j])["cx"]).as<float>(),
            ((parts_data[j])["cy"]).as<float>()
            );
        }
        product_database_[((product_data[i])["id"]).as<int>()] = p; 
    }
    
}

ProductDB::Product ProductDB::getProduct(int product_id){
    auto it = product_database_.find(product_id);
    if (it != product_database_.end()){
        return *(it->second);
    }
    else{
        ProductPtr emptyPtr = ProductPtr(new ProductDB::Product());
        return *emptyPtr;
    }
}

void ProductDB::Product::addProdName(std::string name){
    prod_name_ = name;
}

void ProductDB::Product::addPart(std::string name, float cx, float cy){
    if(parts_.size() == 0){
        ProductDB::Part newPart;
        newPart.part_name = name;
        newPart.pose_x = cx;
        newPart.pose_y = cy;
        newPart.quantity = 1;
        parts_.push_back(newPart);
    }
    else{
        bool partFound = false;
        for(std::vector<ProductDB::Part>::iterator itr=parts_.begin();itr!=parts_.end();++itr){
            // condition to avoid adding same part twice
            if(itr->part_name.compare(name) == 0){
                partFound =true;
                itr->quantity ++;
                break;
            }
        }
        if(partFound == false){
            ProductDB::Part newPart;
            newPart.part_name = name;
            newPart.pose_x = cx;
            newPart.pose_y = cy;
            newPart.quantity = 1;
            parts_.push_back(newPart);
        } 
    }
}