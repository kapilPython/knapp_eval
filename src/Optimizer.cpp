#include "knapp_eval/Optimizer.h"
#include <thread>
#include <math.h>

Optimizer::Optimizer(std::string path){
    pdb_.create_database(path);
    file_path_ = path;
}

std::vector<Optimizer::ProdPart> Optimizer::path_optimizer(unsigned long int order_id,
                                float curr_pose_x,
                                float curr_pose_y){
    //std::vector<Order> srchOrder;
    /*
    Approach 1 for multi-threading
    std::vector<std::string> file_names;
    file_names.push_back("orders_20201201.yaml");
    file_names.push_back("orders_20201202.yaml");
    file_names.push_back("orders_20201203.yaml");
    file_names.push_back("orders_20201204.yaml");
    file_names.push_back("orders_20201205.yaml");
    std::vector<std::thread> th_arr;

    for(std::vector<std::string>::iterator itr=file_names.begin();itr!=file_names.end();++itr){
        Order f;
        th_arr.emplace_back(std::thread(&Optimizer::order_finder,
                                    this,     
                                    file_path_, 
                                    *itr, 
                                    order_id, 
                                    &f));
        srchOrder.push_back(f);
        //th_arr.push_back(move(th));
    }

    for(auto& th : th_arr){
        th.join();
    }
    */
    /*
    Approach 2 for multi-threading
    Order f1;
    srchOrder.push_back(f1);
    std::thread th1 = std::thread(&Optimizer::order_finder,
                                this,     
                                file_path_, 
                                "orders_20201201.yaml", 
                                order_id, 
                                &f1);
    Order f2;
    srchOrder.push_back(f2);
    std::thread th2 = std::thread(&Optimizer::order_finder,
                                this,     
                                file_path_, 
                                "orders_20201202.yaml", 
                                order_id, 
                                &f2);
    Order f3;
    srchOrder.push_back(f3);
    std::thread th3 = std::thread(&Optimizer::order_finder,
                                this,     
                                file_path_, 
                                "orders_20201203.yaml", 
                                order_id, 
                                &f3);
    Order f4;
    srchOrder.push_back(f4);
    std::thread th4 = std::thread(&Optimizer::order_finder,
                                this,     
                                file_path_, 
                                "orders_20201204.yaml", 
                                order_id, 
                                &f4);
    Order f5;
    srchOrder.push_back(f5);
    std::thread th5 = std::thread(&Optimizer::order_finder,
                                this,     
                                file_path_, 
                                "orders_20201205.yaml", 
                                order_id, 
                                &f5);

    th1.join();
    th2.join();
    th3.join();
    th4.join();
    th5.join();

    int vecIndex = 0;
    for (int i=0; i < srchOrder.size();i++){
        if(srchOrder[i].order_found){
            vecIndex = i;
            break;
        }
    }
    std::cout<<vecIndex;
    return true;*/
    // Find Order
    Order f1;
    std::thread th1 = std::thread(&Optimizer::order_finder,
                                this,     
                                file_path_, 
                                "orders_20201201.yaml", 
                                order_id, 
                                &f1);
    th1.join();

    // Convert order into Prodcut Part Vector
    std::vector<ProdPart> unopt_prod_list;
    for (auto prod : f1.products){
        auto prod_part = pdb_.getProduct(prod);
        for(auto part : prod_part.parts_){
            ProdPart pp;
            pp.prod_name = prod_part.prod_name_;
            pp.part_name = part.part_name;
            pp.pose_x = part.pose_x;
            pp.pose_y = part.pose_y;
            unopt_prod_list.push_back(pp);
        }
    }
    
    // Find Shortest Route for all parts wrt initial position
    auto opt_prod_list = product_part_route_optimizer(unopt_prod_list,
                                                        curr_pose_x,
                                                        curr_pose_y);

    ProdPart ppDest;
    ppDest.prod_name = "Destination";
    ppDest.part_name = "Destination";
    ppDest.pose_x = f1.dest_x;
    ppDest.pose_y = f1.dest_y;
    opt_prod_list.push_back(ppDest);

    // return the optimized product part list wrt shortest distance
    return opt_prod_list;
    /*if(f1.order_found){
        return true;
    }
    else{
        return false;
    }*/
}

// algorithm implementing nearest neighbour
std::vector<Optimizer::ProdPart> Optimizer::product_part_route_optimizer(std::vector<Optimizer::ProdPart> ppList,
                                                                            float curr_pose_x,
                                                                            float curr_pose_y)
{
    std::vector<Optimizer::ProdPart> optimized_vec;
    ProdPart initPart;
    initPart.pose_x = curr_pose_x;
    initPart.pose_y = curr_pose_y;
    int tempIndex = 0;
    float shortest_dist = 0.0;
    // find nearest point from init point
    for (size_t i=0; i < ppList.size(); i++){
        if(i == 0){
            shortest_dist = getDistance(initPart, ppList[i]);
            tempIndex = i;
        }
        else{
            float fnd_shortest_dist = getDistance(initPart, ppList[i]);
            if(fnd_shortest_dist < shortest_dist){
                tempIndex = i;
                shortest_dist = fnd_shortest_dist;
            }
        }
    }
    optimized_vec.push_back(ppList[tempIndex]);
    ppList.erase(ppList.begin() + tempIndex);

    // based on the elements inserted in optimized vector keep finding the nearest points
    while(ppList.size() != 0){
        int tempIndex = 0;
        float shortest_dist = 0.0;
        for (size_t i=0; i < ppList.size(); i++){
            if(i == 0){
                shortest_dist = getDistance(optimized_vec[optimized_vec.size() -1],ppList[i]);
            }
            else{
                float fnd_shortest_dist = getDistance(optimized_vec[optimized_vec.size()-1], ppList[i]);
                if(fnd_shortest_dist < shortest_dist){
                    tempIndex = i;
                    shortest_dist = fnd_shortest_dist;
                }
            }
        }
        optimized_vec.push_back(ppList[tempIndex]);
        ppList.erase(ppList.begin() + tempIndex);
    }

    return optimized_vec;
}

float Optimizer::getDistance(Optimizer::ProdPart pp1, Optimizer::ProdPart pp2){
    float x_d = (pp1.pose_x - pp2.pose_x);
    float y_d = (pp1.pose_y - pp2.pose_y);
    return std::sqrt((x_d*x_d) + (y_d*y_d));
}

void Optimizer::create_prod_part_vec(std::string prd_name,
                                    std::string prt_name,
                                    float pose_x,
                                    float pose_y,
                                    std::vector<Optimizer::ProdPart>* ppList)
{
    ProdPart pp;
    pp.prod_name = prd_name;
    pp.part_name = prt_name;
    pp.pose_x = pose_x;
    pp.pose_y = pose_y;
    ppList->push_back(pp);
}

void Optimizer::order_finder(std::string path, 
                                        std::string file_name, 
                                        unsigned long int order_id,
                                        Order* ord){
    std::string final_path = path + "/orders/" + file_name;
    YAML::Node order_data = YAML::LoadFile(final_path);
    unsigned long int n = order_data.size();
    long int index = binary_search(order_data,0,n-1,order_id);
    if (index == -1){
        Order o;
        o.order_found = false;
        *(ord) = o;
        //*(ord).order_found = false;
    }
    else{
        Order o;
        o.order_found = true;
        o.order_id = ((order_data[index])["order"]).as<unsigned long int>();
        o.dest_x = ((order_data[index])["cx"]).as<float>();
        o.dest_y = ((order_data[index])["cy"]).as<float>();
        YAML::Node product_list = order_data[index]["products"];
        for (unsigned int j = 0; j < product_list.size(); j++){
            o.products.push_back((product_list[j]).as<int>());
        } 
        *(ord) = o;   
        //return o;  
    }
}

long int Optimizer::binary_search(YAML::Node data, 
                                    unsigned long int l, 
                                    unsigned long int r, 
                                    unsigned long int order){
    while (l <= r) { 
        unsigned long int m = l + (r - l) / 2; 
  
        // Check if x is present at mid 
        if (((data[m])["order"]).as<unsigned long int>() == order) 
            return m; 
  
        // If x greater, ignore left half 
        if (((data[m])["order"]).as<unsigned long int>() < order) 
            l = m + 1; 
  
        // If x is smaller, ignore right half 
        else
            r = m - 1; 
    } 
  
    // if we reach here, then element was 
    // not present 
    return -1; 
}
