#include <iostream>
#include <map>
#include <vector>
#include <memory>
class ProductDB{
    protected:
        // Structure to hold parts information
        struct Part{
            std::string part_name;
            float pose_x;
            float pose_y;
            int quantity;
        };

        // Inner class which acts as object for adding Products to the Database
        class Product{
            public:
                std::string prod_name_;
                std::vector<ProductDB::Part> parts_;
                void addProdName(std::string name);
                void addPart(std::string name, float cx, float cy);
        };

        typedef std::shared_ptr<Product> ProductPtr;
        // map data structure for the Database
        std::map<int,ProductPtr> product_database_;
    
    public:
    // constructor
    ProductDB();
    // function which parses yaml file to generate the database
    void create_database(std::string path);
    // function which returns the Product object on request of order id
    Product getProduct(int product_id);
    /*
        Other methods like update and delete could be also written
        And in the node a service server can be added attached to update and delete method. 
        but it is out of scope for this task
    */
}; 