#include <iostream>
#include <vector>
#include "knapp_eval/ProductDB.h"
#include "yaml-cpp/yaml.h"
#include "yaml-cpp/exceptions.h"

class Optimizer{
    private:
        ProductDB pdb_; // Object of Product database
        std::string file_path_; // variable to save absolute path
        
        // Structure to holde order data
        struct Order
        {
            bool order_found;
            unsigned long int order_id;
            float dest_x;
            float dest_y;
            std::vector<int> products; 
        };

        // Structue to make list of Products with respect to their parts
        struct ProdPart
        {
            std::string prod_name;
            std::string part_name;
            float pose_x;
            float pose_y;   
        };
    public:
        //constructor
        Optimizer(std::string path);
        
        // Function being called from node which does all the processing
        std::vector<ProdPart> path_optimizer(unsigned long int order_id,
                            float curr_pose_x,
                            float curr_pose_y);

        // Function to find order from yaml file
        void order_finder(std::string path, 
                            std::string file_name, 
                            unsigned long int order_id,
                            Order *o);
        
        // Binary search algorithm to find order id in yaml file
        long int binary_search(YAML::Node data, 
                                        unsigned long int l, 
                                        unsigned long int r, 
                                        unsigned long int order);

        // Method to create Product part list
        void create_prod_part_vec(std::string prd_name,
                                    std::string prt_name,
                                    float pose_x,
                                    float pose_y,
                                    std::vector<ProdPart>* ppList);

        // Route Optimizer function to find shortest route
        std::vector<ProdPart> product_part_route_optimizer(std::vector<ProdPart> ppList,
                                                            float curr_pose_x,
                                                            float curr_pose_y);

        // Function to get distance between two ProductPart objects
        float getDistance(ProdPart pp1, ProdPart pp2);
};